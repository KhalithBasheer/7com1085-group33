# Research Question - Group 33

Members: Khalith Basheer. Bhaskardeep Mathangi. Aishvariya Priya Rathina Sabapathi. Thembavani Elangovan. Minal Mahajan.

Topic: Human Computer Interaction

RQ: Do virtual assistants affect the way students interact with each other?

Context: Behavioural changes.

Population: University Students using Virtual Assistants.

Intervention: AI Technology.

Comparison: Students using Virtual Assistants vs Students not using Virtual Assistants.

Outcome: Social behaviour.


